// console.log("Hello world");

// Scenario: Let's create a simple program that will perform the CRUD Operation for posting the available movies to watch.

/*
	Thor: Love and Thunder
	2022 | Action/Adventure

	Doctor Strange in the Multiverse of Madness
	2022 | Action/Adventure

	Minions: The Rise of Gru
	2022 | Comedy/Adventure

	Black Panther: Wakanda Forever
	2022 | Action Adventure
	
*/

// Mock datbase
let posts = [];
// Posts ID
let count = 1;

// Reactive DOM with JSON (CRUD Operation)

// ADD POST DATA
// This will trigger an event that will add a new post in our mock database upon clicking the "Create" button.
document.querySelector("#form-add-post").addEventListener("submit", (e) => {

	// Prevents the page from reloading. Also prevents the default behavior of our event.
	e.preventDefault();

	posts.push({
		// "id" property will be used for the unique ID of each posts.
		id: count,
		// "title" and "body" values will come from the "form-add-post" input elements.
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	});

	// count will increment everytime a new movie is posted.
	count++

	console.log(posts);
	alert("Movie Successfully Added!");

	showPosts();

});


// RETRIEVE POST/VIEW POST
const showPosts = () => {
	//Create a varaible that will contain all the posts.
	let postEntries = "";


	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	});

	// To check what is stored in the postEntries variables.
	console.log(postEntries)

	// To assign the value of postEntries to the element with "div-post-entries" id.
	document.querySelector("#div-post-entries").innerHTML = postEntries

};

// Edit post

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
}


// Update post

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault()

	for(let i = 0; i < posts.length; i++){

		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;

			showPosts(posts);
			alert('Successfully updated.');

			break;

		}
	}
});

const deletePost = (id) => {
	for(let i=0; i < posts.length; i++){
        if(posts[i].id.toString() === id){
            posts.splice(i, 1);

            showPosts(posts)
            break
        }}}